namespace IwsrArchive.Exceptions;

public class EntityNotFoundException<T> : Exception
{
    public EntityNotFoundException() 
        : base($"{typeof(T)} not found.") { }
    
    public EntityNotFoundException(string message) 
        : base($"{typeof(T)} not found. {message}") { }
    
    public EntityNotFoundException(Guid id) 
        : base($"{typeof(T)} not found. {{id}}") { }
    
    public EntityNotFoundException(string message, Exception innerException)
        : base($"{typeof(T)} not found. {message}", innerException) { }

}