namespace IwsrArchive.Exceptions;

public class InvalidRequestException<T> : Exception
{
    public InvalidRequestException(T? param) 
        : base($"Parameter '{param}' of type {typeof(T)} is invalid.") { }
}