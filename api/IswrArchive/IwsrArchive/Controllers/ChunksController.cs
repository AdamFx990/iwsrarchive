using IwsrArchive.Features.Chunks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IwsrArchive.Controllers;

[Route("[controller]")]
[ApiController]
public class ChunksController : ControllerBase
{
    private readonly IMediator _mediator;

    public ChunksController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] UploadChunkCommand request)
    {
        var id = await _mediator.Send(request);
        return Ok(id);
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] DownloadChunkCommand request)
    {
        var entity = await _mediator.Send(request);
        return Ok(entity);
    }
}