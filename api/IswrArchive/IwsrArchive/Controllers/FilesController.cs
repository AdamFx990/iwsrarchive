using IwsrArchive.Features.Files;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IwsrArchive.Controllers;

[Route("[controller]")]
[ApiController]
public class FilesController : ControllerBase
{
    private readonly IMediator _mediator;

    public FilesController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var entity = await _mediator.Send(new DownloadFileCommand { Id = id });
        return Ok(entity);
    }
    
    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] SearchFilesCommand request)
    {
        var entity = await _mediator.Send(request);
        return Ok(entity);
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody] CreateFileCommand request)
    {
        var id = await _mediator.Send(request);
        return Ok(id);
    }
}