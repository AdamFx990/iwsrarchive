using System.Text.Json.Serialization;
using IwsrArchive.Enums;
using IwsrArchive.Exceptions;
using IwsrArchive.Models;
using IwsrArchive.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Features.Chunks;

public class UploadChunkCommand : IRequest<Guid>
{
    public Guid FileId { get; set; }
    public IEnumerable<byte> Bytes { get; set; }
    public int Index { get; set; }

    public class Handler : IRequestHandler<UploadChunkCommand, Guid>
    {
        private readonly IwsrContext _context;

        public Handler(IwsrContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UploadChunkCommand request, CancellationToken cancellationToken)
        {
            var file = await _context.Files.FindAsync(request.FileId);
            if (file == null)
            {
                throw new EntityNotFoundException<File>(request.FileId);
            }

            file.UploadStatus = UploadStatus.IN_PROGRESS;
            var entity = new Chunk
            {
                Data = request.Bytes.ToArray(),
                CreatedDate = DateTime.UtcNow,
                FileId = request.FileId,
                Index = request.Index,
                Size = request.Bytes.LongCount()
            };

            await _context.Chunks.AddAsync(entity, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            var fileSize = await _context.Chunks
                .Where((c) => c.FileId == request.FileId)
                .SumAsync(c => c.Size, cancellationToken);
            if (file.FileSize == fileSize)
            {
                file.UploadStatus = UploadStatus.COMPLETE;
                await _context.SaveChangesAsync(cancellationToken);
            }
            
            return entity.Id;
        }
    }
}