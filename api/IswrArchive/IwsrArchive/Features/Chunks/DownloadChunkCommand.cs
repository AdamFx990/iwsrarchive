using System.Text.Json.Serialization;
using IwsrArchive.Enums;
using IwsrArchive.Exceptions;
using IwsrArchive.Models;
using IwsrArchive.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Features.Chunks;

public class DownloadChunkCommand : IRequest<IEnumerable<byte>>
{
    public Guid FileId { get; set; }
    public int Index { get; set; }

    public class Handler : IRequestHandler<DownloadChunkCommand, IEnumerable<byte>>
    {
        private readonly IwsrContext _context;

        public Handler(IwsrContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<byte>> Handle(DownloadChunkCommand request, CancellationToken cancellationToken)
        {
            var chunk = await _context.Chunks
                .SingleOrDefaultAsync(f => f.FileId == request.FileId && f.Index == request.Index, cancellationToken);
            if (chunk == null)
            {
                throw new EntityNotFoundException<Chunk>(request.FileId);
            }

            return chunk.Data.ToList();
        }
    }
}