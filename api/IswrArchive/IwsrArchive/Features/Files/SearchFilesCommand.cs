using IwsrArchive.Enums;
using IwsrArchive.Exceptions;
using IwsrArchive.Models;
using IwsrArchive.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Features.Files;

public class SearchFilesCommand : IRequest<ICollection<File>>
{

    public class Handler : IRequestHandler<SearchFilesCommand, ICollection<File>>
    {
        private readonly IwsrContext _context;

        public Handler(IwsrContext context)
        {
            _context = context;
        }

        public async Task<ICollection<File>> Handle(SearchFilesCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Files
                .Where(f => f.UploadStatus == UploadStatus.COMPLETE)
                .ToListAsync(cancellationToken);
 

            return entity;
        }
    }
}