using IwsrArchive.Enums;
using IwsrArchive.Exceptions;
using IwsrArchive.Models;
using IwsrArchive.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Features.Files;

public class CreateFileCommand : IRequest<Guid>
{
    public string Name { get; set; }
    public string FileName { get; set; }
    public string ContentType { get; set; }
    public long FileSize { get; set; }
    public DateTime? LastModified { get; set; }
    public string ModesId { get; set; }

    public class Handler : IRequestHandler<CreateFileCommand, Guid>
    {
        private readonly IwsrContext _context;

        public Handler(IwsrContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateFileCommand request, CancellationToken cancellationToken)
        {
            var type = request.ContentType.Split('/')[0] switch
            {
                "video" => MediaTypes.VIDEO,
                "audio" => MediaTypes.AUDIO,
                "text" => MediaTypes.TEXT,
                _ => MediaTypes.UNKNOWN
            };
            var entity = new File
            {
                Name = request.Name,
                FileName = request.FileName,
                MediaType = type,
                ContentType = request.ContentType,
                FileSize = request.FileSize,
                CreatedDate = DateTime.UtcNow,
                LastModified = request.LastModified,
                ModesId = request.ModesId
            };

            await _context.Files.AddAsync(entity, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            
            return entity.Id;
        }
    }
}