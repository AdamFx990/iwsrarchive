using IwsrArchive.Exceptions;
using IwsrArchive.Models;
using IwsrArchive.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Features.Files;

public class DownloadFileCommand : IRequest<File>
{
    public Guid Id { get; set; }

    public class Handler : IRequestHandler<DownloadFileCommand, File>
    {
        private readonly IwsrContext _context;

        public Handler(IwsrContext context)
        {
            _context = context;
        }

        public async Task<File> Handle(DownloadFileCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Files.FindAsync(request.Id, cancellationToken);
            if (entity == null)
            {
                throw new EntityNotFoundException<File>(request.Id);
            }

            return entity;
        }
    }
}