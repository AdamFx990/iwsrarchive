﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IwsrArchive.Migrations
{
    /// <inheritdoc />
    public partial class AddForiegnKey : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "FileSize",
                table: "Files",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Videos_FileId",
                table: "Videos",
                column: "FileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Files_FileId",
                table: "Videos",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Files_FileId",
                table: "Videos");

            migrationBuilder.DropIndex(
                name: "IX_Videos_FileId",
                table: "Videos");

            migrationBuilder.DropColumn(
                name: "FileSize",
                table: "Files");
        }
    }
}
