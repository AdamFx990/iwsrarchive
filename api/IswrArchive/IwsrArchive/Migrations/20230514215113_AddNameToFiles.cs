﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IwsrArchive.Migrations
{
    public partial class AddNameToFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Audio",
                table: "Audio");

            migrationBuilder.RenameTable(
                name: "Audio",
                newName: "Audios");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Videos",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Audios",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Audios",
                table: "Audios",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Videos_ModesId",
                table: "Videos",
                column: "ModesId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Audios_ModesId",
                table: "Audios",
                column: "ModesId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Videos_ModesId",
                table: "Videos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Audios",
                table: "Audios");

            migrationBuilder.DropIndex(
                name: "IX_Audios_ModesId",
                table: "Audios");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Videos");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Audios");

            migrationBuilder.RenameTable(
                name: "Audios",
                newName: "Audio");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Audio",
                table: "Audio",
                column: "Id");
        }
    }
}
