﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IwsrArchive.Migrations
{
    /// <inheritdoc />
    public partial class Addedchunksize : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Size",
                table: "Chunks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Size",
                table: "Chunks");
        }
    }
}
