using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IwsrArchive.Models.Interfaces;

namespace IwsrArchive.Models;

public class Chunk : IEntity
{
    [Key]
    public Guid Id { get; set; }
    
    [ForeignKey("FK_Chunk_FileId")]
    public Guid FileId { get; set; }
    public File File { get; set; } = null!;

    public byte[] Data { get; set; } = null!;
    
    public int Index { get; set; }
    
    public long Size { get; set; }
    
    public DateTime CreatedDate { get; set; }
}