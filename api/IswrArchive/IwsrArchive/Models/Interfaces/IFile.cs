using System.ComponentModel.DataAnnotations;
using IwsrArchive.Enums;
using Microsoft.EntityFrameworkCore;

namespace IwsrArchive.Models.Interfaces;

public interface IFile : IModesEntity
{
    [Required]
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string ModesId { get; set; }
    public UploadStatus UploadStatus { get; set; }
    public ICollection<Chunk> Chunks { get; set; }
}