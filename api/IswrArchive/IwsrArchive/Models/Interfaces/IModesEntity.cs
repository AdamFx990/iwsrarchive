using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace IwsrArchive.Models.Interfaces;

public interface IModesEntity : IEntity
{
     string ModesId { get; set; }
}