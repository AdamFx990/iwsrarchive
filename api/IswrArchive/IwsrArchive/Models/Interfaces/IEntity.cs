using System.ComponentModel.DataAnnotations;

namespace IwsrArchive.Models.Interfaces;

public interface IEntity
{
    [Key]
    [Required]
    public Guid Id { get; set; }
}