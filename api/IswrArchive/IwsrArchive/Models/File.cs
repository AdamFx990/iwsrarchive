using System.ComponentModel.DataAnnotations;
using IwsrArchive.Enums;
using IwsrArchive.Models.Interfaces;

namespace IwsrArchive.Models;

public class File : IFile
{
    [Key]
    public Guid Id { get; set; }
    
    public string FileName { get; set; }
    
    public string ContentType { get; set; }
    
    public MediaTypes MediaType { get; set; }
    
    public long FileSize { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime? LastModified { get; set; }

    public UploadStatus UploadStatus { get; set; }
    public ICollection<Chunk> Chunks { get; set; } = new List<Chunk>();

    public string Name { get; set; }
    
    public string ModesId { get; set; }
}