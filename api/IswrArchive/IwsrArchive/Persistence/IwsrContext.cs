using IwsrArchive.Models;
using Microsoft.EntityFrameworkCore;
using File = IwsrArchive.Models.File;

namespace IwsrArchive.Persistence;

public class IwsrContext : DbContext
{
    protected readonly IConfiguration Configuration;

    public IwsrContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to postgres with connection string from app settings
        options.UseNpgsql(Configuration.GetConnectionString("connectionString"));
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Chunk>()
            .HasIndex(c => c.Index);
    }

    public DbSet<Chunk> Chunks { get; set; }
    public DbSet<File> Files { get; set; }
}