namespace IwsrArchive.Enums;

public enum MediaTypes
{
    UNKNOWN = -1,
    TEXT = 0,
    AUDIO = 1,
    VIDEO = 2
}