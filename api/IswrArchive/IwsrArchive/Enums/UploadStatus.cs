namespace IwsrArchive.Enums;

public enum UploadStatus
{
    NO_DATA = -1,
    IN_PROGRESS = 0,
    COMPLETE = 1
}