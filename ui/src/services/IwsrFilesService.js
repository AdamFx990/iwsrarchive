import axios from 'axios';
import { iwsrApiEndpoint } from '../constants/endpoints';

export const DownloadFile = async (id) => {
    const response = await axios.get(`${iwsrApiEndpoint}/files/${id}`);
    return response;
};

export const GetFiles = async (params) => {
    const response = await axios.get(`${iwsrApiEndpoint}/files`, { params });
    return response;
};

export const CreateFile = async (file) => {
    const response = await axios.post(`${iwsrApiEndpoint}/files`, file, {
        headers: {
            'Access-Control-Allow-Headers': '*', // this will allow all CORS requests
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET', // this states the allowed methods
            'Content-Type': 'application/json' // this shows the expected content type
        }
    });
    return response;
};
