import axios from 'axios';
import { iwsrApiEndpoint } from '../constants/endpoints';

export const UploadChunk = async (chunk) => {
    const response = await axios.post(`${iwsrApiEndpoint}/chunks`, chunk);
    return response;
};

export const DownloadChunk = async (fileId, index) => {
    const response = await axios.get(`${iwsrApiEndpoint}/chunks`, {
        params: { fileId, index }
    });
    return response;
};
