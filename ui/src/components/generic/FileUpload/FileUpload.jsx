import React, { useState } from 'react';

const FileUpload = ({ id = 'FileUpload', onChange, ...props }) => {

    

    return (
        <div>
            <input id={id} type='file' onChange={onChange} />
        </div>
    );
};

export default FileUpload;
