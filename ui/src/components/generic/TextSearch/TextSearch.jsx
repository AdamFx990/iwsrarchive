import React from 'react';
import './TextSearch.css';

const TextSearch = ({
    id = 'text-search',
    value,
    label,
    onChange,
    ...props
}) => {
    return (
        <div id={id} className='text-search-container'>
            {label && (
                <label id={`${id}-label`} htmlFor={id}>
                    {label}
                </label>
            )}

            <div className='text-search'>
                <input
                    {...props}
                    id={`${id}-input`}
                    type='text'
                    className='text-search-input'
                    name={id}
                    value={value}
                    onChange={onChange}
                />
                <div>svg</div>
            </div>
        </div>
    );
};

export default TextSearch;
