import React from 'react';
import './TextInput.css';

const TextInput = ({ id = 'TextInput', onChange, value, label, ...props }) => {
    return (
        <div>
            {label && <label id={`${id}-label`} htmlFor={id}>{label}</label>}

            <input
                id={id}
                name={id}
                className='text-input'
                type='text'
                onChange={onChange}
                value={value}
                {...props}
            />
        </div>
    );
};

export default TextInput;
