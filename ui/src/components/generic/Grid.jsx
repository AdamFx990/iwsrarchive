import React from 'react';
import Button from './Button/Button';

const Header = ({ x, children }) => {
    return (
        <div id={`header(${x})`} style={{ backgroundColor: '#d7d7d7' }}>
            <span style={{ fontWeight: 'bold' }}>{children}</span>
        </div>
    );
};

const Cell = ({ x, y, children, type, onClick, data, disabled = false }) => {
    let component = <div />;

    const handleClick = () => {
        if (typeof onClick === 'function') {
            onClick(data, y);
        }
    };

    switch (type) {
        case 'button':
            component = (
                <Button
                    onClick={handleClick}
                    value={children}
                    disabled={disabled}
                />
            );
            break;

        case 'string':
            component = (
                <span
                    style={{
                        fontWeight: '500',
                        cursor:
                            typeof onClick === 'function'
                                ? 'pointer'
                                : 'default'
                    }}
                >
                    {children}
                </span>
            );
            break;

        default:
            console.warn(`Invalid type '${type}' for column ${x}.`);
            break;
    }
    return <div id={`cell(${x},${y})`}>{component}</div>;
};

const Row = ({ columns = [], y, data }) => {
    return (
        <>
            {columns.map((c, x) => (
                <Cell
                    key={x}
                    y={y}
                    x={x}
                    type={c.type}
                    onClick={c.onClick}
                    data={data}
                    disabled={c.disabled}
                >
                    {data[c.field]}
                </Cell>
            ))}
        </>
    );
};

const Grid = ({ id = 'Grid', data = [], columns = [] }) => {
    return (
        <div
            id={id}
            style={{
                display: 'grid',
                gridTemplateColumns: `repeat(${columns.length}, minmax(max-content, auto))`,
                width: '100%',
                gap: 5
            }}
        >
            {columns.map((c, x) => (
                <Header key={x} x={x}>
                    {c.name}
                </Header>
            ))}

            {data.map((r, y) => (
                <Row data={r} y={y} columns={columns} key={y} />
            ))}
        </div>
    );
};

export default Grid;
