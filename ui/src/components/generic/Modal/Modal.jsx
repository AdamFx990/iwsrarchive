import React from 'react';
import { createPortal } from 'react-dom';
import './Modal.css';

const Modal = ({ id = 'Modal', visible = false, children, onClose }) => {
    return visible
        ? createPortal(
              <div className='overlay'>
                  <div id={id} className='modal' role='dialog'>
                      {children}
                  </div>
              </div>,
              document.body
          )
        : null;
};

export default Modal;
