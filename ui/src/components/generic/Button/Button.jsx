import React from 'react';

const Button = ({ id, onClick, value, disabled = false }) => {
    return (
        <input
            id={id}
            type='button'
            value={value}
            onClick={onClick}
            disabled={disabled}
        />
    );
};
export default Button;
