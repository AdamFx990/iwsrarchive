import React, { useState } from 'react';
import Modal from './Modal/Modal';
import Button from './Button/Button';
import TextInput from './TextInput/TextInput';
import FileUpload from './FileUpload/FileUpload';
import { CreateFile } from '../../services/IwsrFilesService';
import { DateTime } from 'luxon';
import { UploadChunk } from '../../services/IwsrChunksService';

const _chunkSize = 16 * 1024;

const UploadModal = ({ id = 'UploadModal', visible = false, onClose }) => {
    const [name, setName] = useState('');
    const [modesId, setModesId] = useState('');
    const [uploadProgress, setUploadProgress] = useState(-1);
    const [timeElapsed, setTimeElapsed] = useState(0);
    const [transferRate, setTransferRate] = useState(0);
    const [selectedFile, setSelectedFile] = useState();
    const [chunks, setChunks] = useState();

    const onNameChange = (event) => {
        setName(event.target.value);
    };

    const onModesIdChange = (event) => {
        setModesId(event.target.value);
    };

    const onSelectedFileChange = async (event) => {
        const f = event.target.files[0];
        let i = 0;
        const cs = [];
        while (i <= Math.ceil(f.size / _chunkSize, _chunkSize)) {
            const offset = i * _chunkSize;
            const chunkEnd = offset + _chunkSize;
            const c = f.slice(offset, chunkEnd);
            // const buffer = await c.arrayBuffer();
            const fileReader = new FileReader();
            fileReader.readAsArrayBuffer(c);
            const byteArray = [];
            fileReader.onloadend = (evt) => {
                if (evt.target.readyState == FileReader.DONE) {
                    const arrayBuffer = evt.target.result,
                        array = new Uint8Array(arrayBuffer);

                    for (let i = 0; i < array.length; i++) {
                        byteArray.push(array[i]);
                    }
                }
            };
            // const view = new Uint8Array(buffer);
            cs.push(byteArray);
            i++;
        }
        setChunks(cs);
        // const reader = new FileReader();
        // reader.readAsArrayBuffer(f);
        // reader.onloadend = (evt) => {
        //     if (evt.target.readyState == FileReader.DONE) {
        //         const arrayBuffer = evt.target.result,
        //             array = new Uint8Array(arrayBuffer);

        //         const fileByteArray = [];
        //         for (let i = 0; i < array.length; i++) {
        //             fileByteArray.push(array[i]);
        //         }

        //         const c = [];
        //         for (let i = 0; i < fileByteArray.length; i += _chunkSize) {
        //             c.push(fileByteArray.slice(i, i + _chunkSize));
        //         }
        //         setChunks(c);
        //     }
        // };
        setSelectedFile(f);
        setName(f.name);
    };

    const onUploadClick = async () => {
        setUploadProgress(0);
        const started = DateTime.now();
        const lastModified = DateTime.fromMillis(
            selectedFile.lastModified
        ).toUTC();
        const response = await CreateFile({
            name,
            modesId: modesId,
            fileName: selectedFile.name,
            contentType: selectedFile.type,
            fileSize: selectedFile.size,
            lastModified
        });
        const fileId = response.data;
        const pctInterval = 100 / chunks.length;
        for (let i = 0; i < chunks.length; i++) {
            const chunk = chunks[i];
            await UploadChunk({ fileId, index: i, bytes: chunk });
            const now = DateTime.now();
            const ms = now - started;
            const s = ms / 1000;
            const rate = (i * _chunkSize) / 1024 / s;
            setTimeElapsed(Math.round(s));
            setTransferRate(rate);
            setUploadProgress(Math.round(i * pctInterval));
        }
        onClose();
        setUploadProgress(-1);
        setTimeElapsed(0);
    };

    return (
        <Modal id={id} visible={visible} onClose={onClose}>
            <h1>Upload a file</h1>

            <TextInput value={name} onChange={onNameChange} label='Name *' />

            <TextInput
                value={modesId}
                onChange={onModesIdChange}
                label='Modes ID *'
            />

            <div
                style={{
                    display: 'grid',
                    gridTemplateColumns: 'repeat(3, minmax(max-content, auto))'
                }}
            >
                <FileUpload
                    value={selectedFile}
                    onChange={onSelectedFileChange}
                />
                <span>Progress: {uploadProgress}%</span>
                <span>
                    File size: {(selectedFile?.size / 1024 / 1024).toFixed(2)}{' '}
                    MiB
                </span>
                <span>Chunk size: {_chunkSize / 1024} KiB</span>
                <span>Time elapsed: {timeElapsed} Seconds</span>
                <span>
                    Transfer rate: {((transferRate / 1024) * 8).toFixed(2)} Mbps
                </span>
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <Button
                    value='Upload'
                    onClick={onUploadClick}
                    disabled={uploadProgress >= 0 || chunks === undefined}
                />

                <Button value='Close' onClick={onClose} />
            </div>
        </Modal>
    );
};

export default UploadModal;
