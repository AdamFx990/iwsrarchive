import React, { useContext, useEffect, useState } from 'react';
import Grid from '../generic/Grid';
import { IwsrContext } from '../../contexts/IwsrContext';
import Button from '../generic/Button/Button';
import UploadModal from '../generic/UploadModal';
import { DownloadFile } from '../../services/IwsrFilesService';
import { DownloadChunk } from '../../services/IwsrChunksService';

const VideosPage = ({ id = 'VideosPage', ...props }) => {
    const [createShowing, setCreateShowing] = useState(false);
    const { files, getVideos } = useContext(IwsrContext);
    const [currentVideo, setCurrentVideo] = useState();

    useEffect(() => {
        getVideos();
    }, []);

    const toggleCreateModal = () => {
        setCreateShowing(!createShowing);
    };

    const downloadFile = async (data) => {
        const response = await DownloadFile(data.id);
        const f = response.data;
        let buffer = new ArrayBuffer(f.fileSize);
        let bytes = new Uint8Array(buffer);
        let cId = 0;
        let i = 0;
        const chunks = [];
        while (i < bytes.length) {
            const c = await DownloadChunk(data.id, cId);
            cId++;
            const d = new Uint8Array(c.data);
            for (let j = 0; j < d.length; j++) {
                const b = d[j];
                bytes[i] = b;
                i++;
            }
            const blob = new Blob([bytes]);
            chunks.push(blob);
        }
        console.log('bytes length', bytes.length, f.fileSize);
        const blob = new File(chunks, f.fileName, { type: f.contentType, lastModified: f.lastModified });
        const href = URL.createObjectURL(blob);
        setCurrentVideo(blob);
        const link = document.createElement('a');
        link.href = href;
        link.setAttribute('download', data.name);
        document.body.appendChild(link);
        link.click();
    }

    return (
        <div id={id} {...props}>
            <h1>Videos</h1>

            {currentVideo && <video src={currentVideo} />}

            <Button onClick={getVideos} value='Refresh' />
            <Button onClick={toggleCreateModal} value='Upload' />

            <Grid
                data={files}
                columns={[
                    {
                        name: 'Name',
                        field: 'name',
                        type: 'string',
                    },
                    {
                        name: 'Modes ID',
                        field: 'modesId',
                        type: 'string',
                    },
                    {
                        name: 'Download',
                        field: 'fileName',
                        type: 'button',
                        onClick: downloadFile
                    }
                ]}
            />
            <UploadModal visible={createShowing} onClose={toggleCreateModal}>
                <h1>Upload a file</h1>
            </UploadModal>
        </div>
    );
};

export default VideosPage;
