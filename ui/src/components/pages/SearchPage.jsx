import React, { useContext } from 'react';
import TextSearch from '../generic/TextSearch/TextSearch';
import { IwsrContext } from '../../contexts/IwsrContext';
import Button from '../generic/Button/Button';

const SearchPage = ({ ...props }) => {
    const { searchField, updateSearchField } = useContext(IwsrContext);

    /**
     * Fired by a change to the search field
     * @param {Event} event - text input event
     */
    const onSearchChange = (event) => {
        updateSearchField(event.target.value);
    };

    const onSearchClick = () => {};

    return (
        <div {...props}>
            <h1>Search</h1>

            <TextSearch value={searchField} onChange={onSearchChange} />

            <Button id="search-btn" value="Search" />
        </div>
    );
};

export default SearchPage;
