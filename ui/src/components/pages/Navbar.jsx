import React from 'react';
import { Link, Outlet } from 'react-router-dom';

const Navbar = ({ id = 'Navbar', children, ...props }) => {
    return (
        <>
            <nav id={id} {...props}>
                <ul style={{ display: 'grid', gridAutoFlow: 'column', gridTemplateColumns: 'minmax(max-content, auto)' }}>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/files">Videos</Link>
                    </li>
                    <li>
                        <Link to="/audio">Audio</Link>
                    </li>
                </ul>
            </nav>
            <Outlet />
        </>
    );
};
export default Navbar;
