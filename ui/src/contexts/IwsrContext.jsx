import React, { createContext, useState } from 'react';
import { GetFiles } from '../services/IwsrFilesService';

const IwsrContext = createContext();

const IwsrProvider = ({ children }) => {
    const [searchField, setSearchField] = useState('');
    const [files, setFiles] = useState([]);

    const updateSearchField = (value) => {
        console.info('Search field changed', value);
        setSearchField(value);
    };

    const getVideos = async () => {
        const response = await GetFiles();
        setFiles(response.data);
    };

    return (
        <IwsrContext.Provider
            value={{
                searchField,
                updateSearchField,
                files,
                getVideos
            }}
        >
            {children}
        </IwsrContext.Provider>
    );
};

export { IwsrContext, IwsrProvider };
