import React from 'react';
import './App.css';
import SearchPage from './components/pages/SearchPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { IwsrProvider } from './contexts/IwsrContext';
import NotFoundPage from './components/pages/NotFoundPage';
import Navbar from './components/pages/Navbar';
import VideosPage from './components/pages/VideosPage';

function App() {
    return (
        <IwsrProvider>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Navbar />}>
                        <Route index element={<SearchPage />} />
                        <Route path='files' element={<VideosPage />} />
                        <Route path="*" element={<NotFoundPage />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </IwsrProvider>
    );
}

export default App;
